# hanmatek_psu

python package to control HANMATEK PSU (e.g. HM310T) via its MODBUS interface transported via USB-serial.

also includes a simple Gtk3 gui.

## install

to install this package, clone it and 

    $ git clone https://gitlab.com/fastflo/hanmatek_psu.git
    $ pip install hanmatek_psu/

## gui

![](./screenshots/shot1.png)
![](./screenshots/shot3.png)

to start the simple gui:

	$ hanmatek_psu_gui

## python api

the `hanmatek_psu` python package only provides a single
`hanmatek_psu`-class, expecting a `port`-string as ctor argument
(e.g. `/dev/ttyUSB0`).

```python

import hanmatek_psu

psu = hanmatek_psu.hanmatek_psu("/dev/ttyUSB0")
print("measured voltage: %.2fV" % psu.get_voltage_msr())
```

an object of this class provides these self-explanatory methods:

```python
def get_voltage_msr() # -> float
def get_current_msr() # -> float
def get_power_msr() # -> float
def get_power_cal() # -> float

def set_voltage_cmd(float)
def get_voltage_cmd() # -> float
def set_current_cmd(float)
def get_current_cmd() # -> float

def set_output_enable(bool)
def get_output_enable() # -> bool

def set_protection_voltage(float)
def get_protection_voltage() # -> float
def set_protection_current(float)
def get_protection_current() # -> float
def set_protection_power(float)
def get_protection_power() # -> float

def set_key_beeper(bool)
def get_key_beeper() # -> bool

def get_protection_status() # -> dict of ovp=bool, ocp=bool, ...
def get_protection_status_word() # -> uint16
def get_model() # -> uint16
def get_class_detail() # -> uint16
def get_decimals() # -> uint16
```
