#!/usr/bin/python3
"""
modbus protocol is big endian

rear holding register (function code 03)
preset single register (function code 06)
preset multiple register (function code 16)
master read & write multiple registers (function code 23)
"""

# requires python3
import sys
import struct
import time
import array
import fcntl
import unittest

__all__ = [ "modbus_rtu" ]

def hexlify(packet):
    if not isinstance(packet, bytes):
        raise Exception("expect bytes!")
    return " ".join(["%02X" % b for b in packet])

class modbus_rtu(object):
    def __init__(self, port, slave_id, byte_rate=None, debug=False, expect_local_echo=False, value_byte_ordering=">"):
        self.expect_local_echo = expect_local_echo
        self.value_byte_ordering = value_byte_ordering
        self.debug = debug
        if byte_rate is None:
            byte_rate = 115200 / 10.
        self.byte_rate = int(byte_rate)
        self.port = port
        self.slave_id = slave_id

    @staticmethod
    def _get_crc(packet, crc_poly=0xA001):
        if isinstance(packet, str):
            packet = map(ord, packet)
        crc = 0xffff
        for b in packet:
            crc = b ^ crc
            for i in range(8):
                lsb = crc & 1
                crc = crc >> 1
                if lsb:
                    crc = crc ^ crc_poly
        return crc

    @staticmethod
    def _get_packet(slave_id, function_code, data):
        packet = struct.pack(">BB", slave_id, function_code) + data
        crc = modbus_rtu._get_crc(packet)
        return packet + struct.pack("<H", crc) # funny that this has to be little endian!

    def _read_packet(self):
        if self.debug: print("reading header bytes")
        hdr = self.port.read(2)
        if self.debug: print("  got %d header bytes: %s" % (len(hdr), hexlify(hdr)))
        slave_id, fc = struct.unpack("BB", hdr)
        if self.debug: print("  slave_id: %d, function-code: %d" % (slave_id, fc))
        if fc == 16:
            n_bytes = 4
        else:
            n_bytes_byte = self.port.read(1)
            hdr += n_bytes_byte
            n_bytes = ord(n_bytes_byte)
            if self.debug: print("  n_bytes: %d" % (n_bytes))
        payload_crc = self.port.read(n_bytes + 2) # plus crc
        if self.debug: print("  payload with crc: %s" % (hexlify(payload_crc)))
            
        if len(payload_crc) != n_bytes + 2:
            raise Exception("short read: wanted %d bytes got only %d: %s" % (n_bytes + 2, len(payload_crc), hexlify(payload_crc)))
        payload = payload_crc[:-2]
        received_crc = struct.unpack("<H", payload_crc[-2:])[0]
        if self.debug: print("  payload: %s, crc: %#x" % (hexlify(payload), received_crc))
        crc = modbus_rtu._get_crc(hdr + payload)
        if crc != received_crc:
            raise Exception("received invalid crc %#x, expected %#x" % (received_crc, crc))
        return slave_id, fc, payload
    
    def _send_request(self, function_code, data):
        packet = modbus_rtu._get_packet(self.slave_id, function_code, data)
        # todo: ensure atleast 3.5 bytes of silence between end of last packet and start of this one
        if self.debug: print("sending: %s" % hexlify(packet))
        self.port.write(packet)
        if self.expect_local_echo:
            local_echo = self.port.read(len(packet))
            if self.debug: print("echo:    %s" % hexlify(local_echo))
        
    def _wait_response(self, function_code):
        while True:
            slave_id, fc, data = self._read_packet()
            if slave_id != self.slave_id:
                continue
            if fc != function_code:
                continue
            return data
        
    def read_registers(self, first_reg, n_regs=2):
        if self.debug: print("\nread_registers from %#x, n: %d" % (first_reg, n_regs))
        self._send_request(3, struct.pack(">HH", first_reg, n_regs))
        response = self._wait_response(3)
        ret = struct.unpack(self.value_byte_ordering + "%dH" % n_regs, response)
        if self.debug: print("  result: %s" % (", ".join(["%#x" % v for v in ret])))
        if n_regs == 1:
            return ret[0]
        return ret

    def write_registers(self, first_reg, values):
        if self.debug: print("\nwrite_registers from %#x, values %s" % (first_reg, ", ".join(["%#x" % v for v in values])))
        n_regs = len(values)
        req = [ struct.pack(">HHB", first_reg, n_regs, n_regs * 2) ]
        for value in values:
            req.append(struct.pack(self.value_byte_ordering + "H", value))
        self._send_request(16, b"".join(req))
        response = self._wait_response(16)
        first_reg_, n_regs_ = struct.unpack(">HH", response)
        if first_reg_ != first_reg or n_regs_ != n_regs:
            print("warning: wanted to write from reg %#x %d values. response is reg %#x %d values!" % (
                first_reg, n_regs, first_reg_, n_regs_))

    def read_write_registers(self, read_first_reg, read_n_regs, write_first_reg, values):
        if self.debug: print("\nread_write_registers read from %#x, n: %d, write to %#x, %s" % (
                read_first_reg, read_n_regs, write_first_reg, ", ".join(["%#x" % v for v in values])))
        write_n_regs = len(values)
        req = [ struct.pack(">HHHHB", read_first_reg, read_n_regs, write_first_reg, write_n_regs, write_n_regs * 2) ]
        for value in values:
            req.append(struct.pack(self.value_byte_ordering + "H", value))
        self._send_request(23, "".join(req))
        response = self._wait_response(23)
        read_values = struct.unpack(">HH", response)
        ret = struct.unpack(self.value_byte_ordering + "%dH" % read_n_regs, read_values)
        if self.debug: print("  result: %s" % (", ".join(["%#x" % v for v in ret])))
        if read_n_regs == 1:
            return ret[0]
        return ret

class modbus_rtu_tests(unittest.TestCase):
    def test_crc(self):
        self.assertEqual(0x1241, modbus_rtu._get_crc([2, 7]))
        self.assertEqual(0xcec5, modbus_rtu._get_crc([9, 3, 7, 0xd0, 00, 2]))
        self.assertEqual(0x3344, modbus_rtu._get_crc([9, 3, 4, 0xe0, 0, 0, 0]))
        
    def test_packets(self):
        packet = modbus_rtu._get_packet(9, 3, struct.pack(">HH", 2000, 2))
        self.assertEqual(hexlify(packet), "09 03 07 D0 00 02 C5 CE")
        
        packet = modbus_rtu._get_packet(9, 3, struct.pack(">BHH", 4, 0xe000, 0))
        self.assertEqual(hexlify(packet), "09 03 04 E0 00 00 00 44 33")
            
