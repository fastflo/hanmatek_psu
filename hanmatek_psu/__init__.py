import serial
import numpy as np

from .modbus import modbus_rtu


class hanmatek_psu(object):
    rw_registers = dict(
        output_enable=(0x01, None, bool),

        protection_voltage=(0x20, 100),
        protection_current=(0x21, 1000),
        protection_power=(0x22, 1000, float, 2),

        voltage_cmd=(0x30, 100),
        current_cmd=(0x31, 1000),
        time_span=(0x32, 1), # todo: scaling? seconds?,

        key_beeper=0x8804,
    )
    r_registers = dict(
        protection_status_word=0x02,
        model=0x03,
        class_detail=0x04,
        decimals=0x05, # bcd most-significant: voltage, current, power lest-signification

        voltage_msr=(0x10, 100),
        current_msr=(0x11, 1000),
        power_msr=(0x12, 1000, float, 2),

        power_cal=0x14,
    )

    def __init__(self, port="/dev/serial/by-id/usb-1a86_USB_Serial-if00-port0", bps=9600, debug=False):
        self.port = serial.Serial(port, bps, interCharTimeout=0.1, timeout=2)
        self.mb = modbus_rtu(self.port, slave_id=1, byte_rate=int(bps/10), debug=debug)
        self.cache_size = int(0x10000 / 2)
        self.cache = np.zeros((self.cache_size, ), dtype=np.uint16)
        self.cache_valid = np.zeros((self.cache_size, ), dtype=np.uint8)

        def get_setter(reg, nregs, scaling):
            return lambda value: self._set_param(reg, value, scaling, nregs)
        def get_getter(reg, nregs, scaling, dtype):
            def _getter(allow_cache=False):
                return self._get_param(reg, scaling, dtype, nregs, allow_cache=allow_cache)
            return _getter
        for what, regs in (
            ("rw", self.rw_registers),
            ("r", self.r_registers)):
            for name, params in regs.items():
                scaling = None
                dtype = None
                nregs = 1
                if isinstance(params, tuple):
                    if len(params) == 4:
                        reg, scaling, dtype, nregs = params
                    elif len(params) == 3:
                        reg, scaling, dtype = params
                    elif len(params) == 2:
                        reg, scaling = params
                    elif len(params) == 1:
                        reg = params[0]
                else:
                    reg = params
                if "w" in what:
                    setattr(self, "set_" + name, get_setter(reg, nregs, scaling))
                if "r" in what:
                    setattr(self, "get_" + name, get_getter(reg, nregs, scaling, dtype))

    def _fill_cache(self, reg, v):
        if not isinstance(v, (list, tuple)):
            self.cache[reg] = v
            self.cache_valid[reg] = 1
            return
        for iv in v:
            self.cache[reg] = iv
            self.cache_valid[reg] = 1
            reg += 1
    def _in_cache(self, reg, n):
        return min(self.cache_valid[reg:reg + n]) == 1
    def _read_cache(self, reg, n):
        if n == 1:
            return self.cache[reg]
        return tuple(self.cache[reg:reg + n])

    def read_registers(self, reg, nregs, allow_cache=False):
        if allow_cache and self._in_cache(reg, nregs):
            return self._read_cache(reg, nregs)
        v = self.mb.read_registers(reg, nregs)
        self._fill_cache(reg, v)
        return v
    def write_registers(self, reg, v):
        self.mb.write_registers(reg, v)
        self._fill_cache(reg, v)

    def _set_param(self, reg, v, scaling=None, nregs=1):
        if scaling is not None:
            v = v * scaling
        if isinstance(v, bool):
            v = 1 if v else 0
        v = int(v)
        if nregs == 1:
            v = [v]
        else:
            vl = []
            for i in range(nregs):
                vl.append(v & 0xffff)
                v = v >> 16
            vl.reverse()
            v = vl
        self.write_registers(reg, v)
        self._fill_cache(reg, v)

    def _get_param(self, reg, scaling=None, dtype=None, nregs=1, allow_cache=False):
        if allow_cache and self._in_cache(reg, nregs):
            v = self._read_cache(reg, nregs)
        else:
            v = self.read_registers(reg, nregs)
            self._fill_cache(reg, v)
        if nregs > 1:
            vv = 0
            for vi in v:
                vv = (vv << 16) | vi
            v = vv
        if scaling is not None:
            v = v / scaling
        if dtype is not None:
            v = dtype(v)
        return v

    def get_protection_status(self, allow_cache=False):
        w = self.get_protection_status_word(allow_cache=allow_cache)
        stat = dict(
            ovp=1 << 0, # over voltage protection
            ocp=1 << 1, # over current protection
            opp=1 << 2, # over power protection
            otp=1 << 3, # over temperature protection
            scp=1 << 4, # short-circuit protection
        )
        for name, mask in list(stat.items()):
            stat[name] = bool(w & mask)
        return stat

    def dump(self, n_regs=64, per_row=16, start=0x01):
        all_words = []
        while n_regs > 0:
            to_read = min(n_regs, 64)
            words = self.mb.read_registers(start, to_read)
            if not isinstance(words, tuple):
                all_words.append(words)
            else:
                all_words.extend(words)
            n_regs -= to_read
            start += to_read
        out = []
        for i in range(0, len(all_words), per_row):
            out.append("%04x: %s" % (i, " ".join(["%04x" % w for w in all_words[i:i+per_row]])))
        return "\n".join(out)

    def get_ucp_measurement(self, allow_cache=False):
        #0x10 4 # vol, cur, pow1, pow2
        if not allow_cache:
            self.read_registers(0x10, 4)
        u = self.get_voltage_msr(allow_cache=True)
        c = self.get_current_msr(allow_cache=True)
        p = self.get_power_msr(allow_cache=True)
        return u, c, p

    def get_uc_commands(self, allow_cache=False):
        if not allow_cache:
            self.read_registers(0x30, 2)
        u = self.get_voltage_cmd(allow_cache=True)
        c = self.get_current_cmd(allow_cache=True)
        return u, c

    def get_enable_and_protection_state(self):
        enable, prot = self.read_registers(0x01, 2)
        prot = self.get_protection_status(allow_cache=True)
        return enable, prot

    def get_protection_commands(self):
        self.read_registers(0x20, 4)
        u = self.get_protection_voltage(allow_cache=True)
        c = self.get_protection_current(allow_cache=True)
        p = self.get_protection_power(allow_cache=True)
        return u, c, p

    def get_measurements(self):
        """
        returns measured voltage, current, power, protection & output-enabled status
        """
        #0x01 2 # enable, prot
        enable, prot = self.read_registers(0x01, 2)
        prot = self.get_protection_status(allow_cache=True)
        u, c, p = self.get_ucp_measurement(allow_cache=False)
        return u, c, p, prot, enable

    def get_state(self):
        mu, mc, mp, prot, enable = self.get_measurements()
        cu, cc = self.get_uc_commands()
        return mu, mc, mp, prot, enable, cu, cc
