#!/usr/bin/python

import os
import sys
import time
import argparse
import threading
import queue
import traceback

import gi
gi.require_version("Gtk", "3.0")
from gi.repository import Gtk
from gi.repository import GLib

import hanmatek_psu

class psu_thread(threading.Thread):
    def __init__(self, port, update_cb):
        threading.Thread.__init__(self)
        self.daemon = True
        try:
            self.psu = hanmatek_psu.hanmatek_psu(port)
            model = self.psu.get_model()
            class_detail = self.psu.get_class_detail()
            print("model: %#x, class_detail: %#x" % (model, class_detail))
        except:
            raise Exception("could not read model from psu!") from sys.exc_info()[1]

        self.keep_running = True
        self.update_cb = update_cb
        self.cmd_queue = queue.Queue()

    def add_cmd(self, cmd, value):
        #print("enqueue command %r value %r" % (cmd, value))
        self.cmd_queue.put((cmd, value))

    def run(self):
        # read queue
        read_queue = [
            ("msr", "get_ucp_measurement"),
            ("cmd", "get_uc_commands"),
            ("enable_prot", "get_enable_and_protection_state"),
            ("prot_cmd", "get_protection_commands")
        ]

        outer = 0
        inner = 1
        while self.keep_running:
            # check cmd queue from gui
            commands = dict()
            while not self.cmd_queue.empty():
                cmd, value = self.cmd_queue.get()
                commands[cmd] = value
            for cmd, value in commands.items():
                getattr(self.psu, cmd)(value)

            if outer % 2 == 0:
                what, method = read_queue[0]
            else:
                what, method = read_queue[inner]
                inner += 1
                if inner == len(read_queue):
                    inner = 1
            outer += 1

            method = getattr(self.psu, method)
            values = method()
            #print(what, values)
            self.update_gui(what, values)

    def stop(self):
        self.keep_running = False
        self.join()

    def update_gui(self, what, value):
        GLib.idle_add(self.update_cb, what, value)

class gui(object):
    def __init__(self, port):
        self.psu = psu_thread(port, update_cb=self.on_update)

        self.builder = Gtk.Builder()
        self.builder.add_from_file(os.path.join(os.path.dirname(__file__), "gui.ui"))
        self.builder.connect_signals(self)

        self.window.show_all()

        self.last_power_cmd = None

        self.psu.start()

        self.is_first_cmd_update = True
        self.is_first_protection_update = True

    def __getattr__(self, name):
        widget = self.builder.get_object(name)
        if widget is not None:
            setattr(self, name, widget)
            return widget
        raise AttributeError(name)

    def on_window_delete_event(self, widget, ev):
        self.psu.stop()
        Gtk.main_quit()

    def on_enable_output_toggled(self, btn):
        active = btn.get_active()
        if active == self.last_power_cmd:
            return True
        self.psu.add_cmd("set_output_enable", 1 if active else 0)

    def on_update(self, what, value):
        if what == "msr":
            mu, mc, mp = value
            self.voltage_msr_label.set_text("%.2f" % mu)
            self.current_msr_label.set_text("%.3f" % mc)
            self.power_msr_label.set_text("%.3f" % mp)
        elif what == "cmd":
            cu, cc = value
            self.voltage_cmd_label.set_text("%.2f" % cu)
            self.current_cmd_label.set_text("%.3f" % cc)
            if self.is_first_cmd_update:
                # also set entries!
                self.voltage_cmd.set_value(cu)
                self.current_cmd.set_value(cc)
                self.is_first_cmd_update = False
        elif what == "enable":
            enable = value
            if enable != self.last_power_cmd:
                self.last_power_cmd = enable
                self.enable_output.set_active(enable)
        elif what == "prot":
            prot = value
            for name, value in prot.items():
                getattr(self, "%s_active" % name).set_active(value)
        elif what == "enable_prot":
            enable, prot = value
            self.on_update("enable", enable)
            self.on_update("prot", prot)
        elif what == "prot_cmd":
            u, c, p = value
            self.protection_voltage_label.set_text("%.2f" % u)
            self.protection_current_label.set_text("%.3f" % c)
            self.protection_power_label.set_text("%.3f" % p)
            if self.is_first_protection_update:
                # also set entries!
                self.protection_voltage.set_value(u)
                self.protection_current.set_value(c)
                self.protection_power.set_value(p)
                self.is_first_protection_update = False
        elif what == "all":
            mu, mc, mp, prot, enable, cu, cc = value
            self.on_update("msr", (mu, mc, mp))
            self.on_update("enable_prot", (enable, prot))
            self.on_update("cmd", (cu, cc))
            # missing prot_cmd
        return False

    def on_voltage_cmd_value_changed(self, sp):
        if self.is_first_cmd_update:
            return
        self.psu.add_cmd("set_voltage_cmd", sp.get_value())

    def on_current_cmd_value_changed(self, sp):
        if self.is_first_cmd_update:
            return
        self.psu.add_cmd("set_current_cmd", sp.get_value())
    def on_protection_voltage_value_changed(self, sp):
        if self.is_first_protection_update:
            return
        self.psu.add_cmd("set_protection_voltage", sp.get_value())
    def on_protection_current_value_changed(self, sp):
        if self.is_first_protection_update:
            return
        self.psu.add_cmd("set_protection_current", sp.get_value())
    def on_protection_power_value_changed(self, sp):
        if self.is_first_protection_update:
            return
        self.psu.add_cmd("set_protection_power", sp.get_value())

    def on_enable_output_toggled(self, btn):
        active = btn.get_active()
        if active == self.last_power_cmd:
            return
        self.psu.add_cmd("set_output_enable", active)
        self.last_power_cmd = active

def main():
    parser = argparse.ArgumentParser(description="simple gui for hanmatek psu's")
    parser.add_argument("--port", default="/dev/serial/by-id/usb-1a86_USB_Serial-if00-port0", help="serial tty where psu is connected to")
    args = parser.parse_args()

    hm_gui = gui(args.port)
    Gtk.main()

if __name__ == "__main__":
    main()
